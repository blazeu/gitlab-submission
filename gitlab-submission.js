const axios = require('axios')
const yaml = require('js-yaml')
const qs = require('querystring')
const crypto = require('crypto')
const util = require('util')

const logs = []
const log = (level, ...msgs) => {
  console.log(...msgs)
  msgs.forEach(msg => logs.push(`${level}: ${util.inspect(msg)}`))
}

const gitlab = token => {
  const client = axios.create({
    baseURL: 'https://gitlab.com/api/v4',
    headers: {
      'PRIVATE-TOKEN': token,
    },
  })

  return {
    createBranch(data) {
      const { project, ...attr } = data
      return client.post(`/projects/${project}/repository/branches`, attr)
    },

    createFile(data) {
      const { path, project, ...attr } = data
      return client.post(`/projects/${project}/repository/files/${path}`, attr)
    },

    createMR(data) {
      const { project, ...attr } = data
      return client.post(`/projects/${project}/merge_requests`, attr)
    },
  }
}

const createSubmission = (provider, project) => async data => {
  const {
    branch,
    author,
    email,
    message,
    title,
    description,
    file,
    content,
  } = data

  const projectEscaped = qs.escape(project)
  const path = qs.escape(file)

  log('info', 'Processing submission', data)

  log('info', 'Creating branch')
  await provider.createBranch({
    project: projectEscaped,
    ref: 'master',
    branch,
  })

  log('info', 'Creating file')
  await provider.createFile({
    author_name: author,
    author_email: email,
    commit_message: message,
    project: projectEscaped,
    branch,
    content,
    path,
  })

  log('info', 'Creating merge request')
  return provider.createMR({
    source_branch: branch,
    target_branch: 'master',
    project: projectEscaped,
    title,
    description,
  })
}

const hash = (string, hash) => {
  const normalized = string.trim().toLowerCase()
  return crypto
    .createHash(hash, '')
    .update(normalized)
    .digest('hex')
}

const randomString = () => {
  return Math.random()
    .toString(36)
    .substring(2)
}

const netlifyFormToSubmission = ({
  branch,
  author,
  email,
  title,
  description,
  identifier,
  path,
  excludeField,
}) => req => {
  const message = `Add new submission from: ${author}`

  const number = req.number
  const paddedNumber = number.toString().padStart(3, '0')
  const file = `${path}/${paddedNumber}.md`

  const data = { nomor: number, ...req.data, submission_id: req.id }

  if (identifier) {
    data.submitter_identifier = identifier
  }

  excludeField.forEach(field => {
    delete data[field]
  })

  const frontmatter = yaml.dump(data)
  const content = `---\n${frontmatter}---\n`

  return {
    branch,
    author,
    email,
    message,
    title,
    description,
    file,
    content,
  }
}

Date.prototype.toLocaleISOString = function(n) {
  const tzoffset = this.getTimezoneOffset()
  const localTime = new Date(this - tzoffset * 60000)
  const localISOTime = localTime.toISOString().slice(0, -1)
  const localOffsetMins = Math.abs(tzoffset % 60)
  const localOffset =
    '' +
    Math.trunc(tzoffset / 60) +
    ':' +
    (localOffsetMins < 10 ? '0' + localOffsetMins : '' + localOffsetMins)
  return localISOTime + localOffset
}

const logWriter = (provider, project, branch = 'master') => async () => {
  const date = new Date().toLocaleISOString()
  const projectEscaped = qs.escape(project)

  await provider.createFile({
    branch,
    author_name: 'logger',
    author_email: 'logger@nobody.io',
    commit_message: `Triggered @ ${date}`,
    project: projectEscaped,
    content: logs.join('\n'),
    path: `${date}.log`,
  })
}

let writeLog

module.exports = async ({ secrets, meta, body }, cb) => {
  log('info', 'New request', body)

  const token = secrets.GITLAB_TOKEN
  const project = meta.PROJECT
  const logProject = meta.LOG_PROJECT
  const logBranch = meta.LOG_BRANCH
  const excludeField = meta.FORM_EXCLUDE || ''
  const hashAlgo = meta.HASH || 'sha256'
  process.env.TZ = meta.TIMEZONE

  const provider = gitlab(token)
  const submit = createSubmission(provider, project)

  if (logProject) {
    writeLog = logWriter(provider, logProject, logBranch)
  }

  const author = body.data[meta.FORM_AUTHOR_KEY]
  const title = body.data[meta.FORM_TITLE_KEY]
  const branch = meta.FORM_BRANCH_KEY
    ? body.data[meta.FORM_BRANCH_KEY]
    : randomString()
  const branchPrefix = meta.BRANCH_PREFIX || ''

  const netlifyForm = netlifyFormToSubmission({
    author,
    branch: branchPrefix + hash(branch, hashAlgo),
    email: body.data[meta.FORM_EMAIL_KEY],
    title: `${body.number} - ${author} - ${title}`,
    description: body.data[meta.FORM_DESCRIPTION_KEY],
    identifier: meta.FORM_IDENTIFIER_KEY
      ? hash(body.data[meta.FORM_IDENTIFIER_KEY], hashAlgo)
      : null,
    path: meta.PATH,
    excludeField: excludeField.split(';'),
  })

  let error

  try {
    await submit(netlifyForm(body))
  } catch (e) {
    error = e.response ? e.response.data : e
    log('error', error.message || error)
  }

  if (writeLog) {
    writeLog()
  }

  cb(null, error)
}

process.on('uncaughtException', err => {
  log('error', err)

  if (writeLog) {
    writeLog()
  }
})

process.on('unhandledRejection', (reason, p) => {
  log('error', reason)
})
