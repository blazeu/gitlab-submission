# GitLab Submission

A small [Webtask](https://webtask.io/) script to create markdown frontmatter
from [Netlify](https://www.netlify.com/) form submission via GitLab Merge
Request.

## Setup

See [Webtask documentation](https://webtask.io/docs/101)

## Configuration

Configuration is editable via Webtask editor.

### Secrets

#### `GITLAB_TOKEN` **required**

GitLab token to create the Merge Request.

### Meta

#### `PATH` **required**

Path to store the file. Example: `posts/comments`.

#### `PROJECT` **required**

GitLab Project to create the Merge Request. Example: `blazeu/awesome-blog`.

#### `FORM_AUTHOR_KEY` **required**

Form field name to be used as the commit author. Example: `user_name`.

#### `FORM_EMAIL_KEY` **required**

Form field name to be used as the commit email.

#### `FORM_TITLE_KEY` **required**

Form field name to be used as the merge request title.

#### `FORM_BRANCH_KEY`

Form field name to be used as the branch. The value is hashed before it is used
to create a branch name. Defaults to random string.

#### `FORM_IDENTIFIER_KEY`

Form field name to be used as `submitter_identifier` in the resulting markdown.
The value is hashed before it is used.

#### `FORM_EXCLUDE`

Form field name to be excluded in the file, separated by semicolon. Example:
`ip;credentials;secrets`.

#### `BRANCH_PREFIX`

Branch name prefix.

#### `LOG_PROJECT`

GitLab Project to write logs. Example: `blazeu/submission-logs`.

#### `LOG_BRANCH`

GitLab Project Branch name to write logs. Defaults to `master`.

#### `TIMEZONE`

Timezone configuration. Used to write logs.

#### `HASH`

Hash algorithm to use. Defaults to `sha256`.
